package corejava;

public class DataTypes {

    public static void main(String[] args) {

        //byte - 8 bit
        byte number1 = -128;
        byte number2 = 127;

        //short - 16 bit
        short number3 = -32_768;
        short number4 = 32_767;

        //int - 32 bit
        int number5 = -2_147_483_648;
        int number6 = 2_147_483_647;

        // float - 32 bit -- f
        float number8 = 1.222f;

        // long - 64bit  ---l
        long number7 = 3535355345252252452l;

        //double -64 bit -(d) --optional
        double number9 = 1.33d; // 1.33d

        //boolean true or false
        boolean x = true;

        //char -- 1 character -- unicode character
        //https://en.wikipedia.org/wiki/List_of_Unicode_characters
        //U+00AE
        char s = 's';
        char y = '\u00ae';
        System.out.println(y);
    }

}
