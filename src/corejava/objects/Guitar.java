package corejava.objects;

public class Guitar {


    // method calling from object context

    public static void main(String[] args) {

        //String name = "ash";

        Guitar referenceVariable = new Guitar(); // object creation
        //className object/referenceVariable = new ClassName ---> constructor ();

        referenceVariable.getMyJackson();

        int bucket = referenceVariable.getMyAge();


    }


    public void getMyJackson() {
        System.out.println("jackson cs ");
    }


    public int getMyAge() {
        return 27;
    }


}
